﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class HandPresence : MonoBehaviour
{
    public InputDeviceCharacteristics controllerCharacteristics;    
    private InputDevice targetDevice;
    public Animator handAnimator;

    void Start()
    {
        Debug.Log("Hello");
		TryInitialize();
    }

    void TryInitialize()
    {
        List<InputDevice> devices = new List<InputDevice>();
		//InputDevices.GetDevices(devices);

	foreach (var item in devices)	
		{
			Debug.Log(item.name + item.characteristics);
		}

        InputDevices.GetDevicesWithCharacteristics(controllerCharacteristics, devices);
        if (devices.Count > 0)
        {
            targetDevice = devices[0];
        }
    }

    void UpdateHandAnimation()
    {
        if(targetDevice.TryGetFeatureValue(CommonUsages.trigger, out float triggerValue))
        {
            Debug.Log("Trigger: " + triggerValue);
			handAnimator.SetFloat("Pinch", triggerValue);
			
        }
        else
        {
            Debug.Log("Trigger: " + triggerValue);
			handAnimator.SetFloat("Pinch", 0);
        }

        if (targetDevice.TryGetFeatureValue(CommonUsages.grip, out float gripValue))
        {
            Debug.Log("Grip: " + gripValue);
			handAnimator.SetFloat("Flex", gripValue);
        }
        else
        {
            Debug.Log("Grip: " + gripValue);
			handAnimator.SetFloat("Flex", 0);
        }

	
    }

    // Update is called once per frame
    void Update()
    {
        if(!targetDevice.isValid)
        {
            TryInitialize();
        }
        else
        {
            UpdateHandAnimation();
        }
    }
}
