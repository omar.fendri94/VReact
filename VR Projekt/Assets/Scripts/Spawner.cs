using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Spawner : MonoBehaviour
{
    public GameObject objectToSpawn;
    public Vector3 spawnLocation;
    public Transform dummyTransform;
    public bool debugSpawn;
    public Animator anim;
    

    public void Start(){
        if(debugSpawn){
            animateDoor();
            spawn();
        }
    }

    public Object spawn(){
        //return Instantiate(objectToSpawn, dummyTransform.position, Quaternion.identity);
        return Instantiate(objectToSpawn, dummyTransform);
    }

    public void spawnNoReference(){
        spawn();
    }

    public void animateDoor(){
        anim.Play("R1_Closing");
        anim.Play("R1_Closed");
        anim.Play("R1_Opening");
        anim.Play("R1_Open");
    }
}

