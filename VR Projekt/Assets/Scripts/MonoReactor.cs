using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoReactor : MonoBehaviour
{
    public int reagent1;
    public int reagent2;
    // rotations taken from prefab in spawner (Quaternion.identity)
    public Spawner reagent1tempSpawner;
    public Spawner reagent2tempSpawner;
    public Spawner transitionSpawner; // make the transition element start its animation in start or update
    public Spawner productSpawner;
    private List<Object> tempObjects = new List<Object>();

    private bool reagent1present = false;
    private bool reagent2present = false;

    public int reactionTime;


    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other){
        GameObject checkerObj = other.gameObject;
        Reagent checker = checkerObj.GetComponent<Reagent>();
        if(checker != null){
            if(checker.getType() == reagent1 && !reagent1present){
                Destroy(checkerObj);
                tempObjects.Add(reagent1tempSpawner.spawn());
                reagent1present = true;
            }
            if(checker.getType() == reagent2 && !reagent2present){
                Destroy(checkerObj);
                tempObjects.Add(reagent2tempSpawner.spawn());
                reagent2present = true;
            }
        }
    }

    public void React(){
        if(reagent1present && reagent2present){
            reagent1present = false;
            reagent2present = false;
            foreach (Object iter in tempObjects){
                Destroy(iter);
            }
            tempObjects.Clear();
            StartCoroutine(ReactCoroutine());

        }
    }

    IEnumerator ReactCoroutine(){
        Object transition = transitionSpawner.spawn();
        yield return new WaitForSeconds(reactionTime);
        Destroy(transition);
        productSpawner.spawn();
    }

}
