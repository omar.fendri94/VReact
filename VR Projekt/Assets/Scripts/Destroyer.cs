using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    private List<GameObject> objectsToDestroy = new List<GameObject>();
    public bool debugMode;
    public int debugCountdown;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(debugMode){
            debugCountdown = debugCountdown - 1;
            if(debugCountdown <= 0){
                cleanGarbage();
            }
        }
    }

    private void OnTriggerEnter(Collider other){
        GameObject checker = other.gameObject;
        Debug.Log(checker + "has entered");
        if(checker.GetComponent<Destructible>() != null){
            Debug.Log(checker + "in if");
            objectsToDestroy.Add(checker);
        }
    }

    private void OnTriggerExit(Collider other){
        GameObject checker = other.gameObject;
        Debug.Log(checker + "has left");
        if(objectsToDestroy.Contains(checker)){
            Debug.Log(checker + "in if left");
            objectsToDestroy.Remove(checker);
        }
    }

    public void cleanGarbage(){
        foreach(GameObject iterate in objectsToDestroy){
            //objectsToDestroy.Remove(iterate);
            Destroy(iterate);
        }
        objectsToDestroy.Clear();
    }
}
