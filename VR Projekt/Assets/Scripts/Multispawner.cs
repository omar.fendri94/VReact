using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multispawner : MonoBehaviour
{
    public List<GameObject> spawnableObjects;
    int indexToSpawn = 0;
    public bool debugPosition;
    public List<Vector3> spawnLocations;
    
    
    public void nextObject()
    {
        if(indexToSpawn == spawnableObjects.Count - 1){
            indexToSpawn = 0;
        }else{
            indexToSpawn ++;
        }
    }

    public void previousObject()
    {
        if(indexToSpawn == 0){
            indexToSpawn = spawnableObjects.Count - 1;
        }else{
            indexToSpawn --;
        }
    }

    public void spawn(){
        Instantiate(spawnableObjects[indexToSpawn], spawnLocations[indexToSpawn], Quaternion.identity);
    }

    void Update(){
        if(debugPosition){
            spawn();
            nextObject();
        }
    }
}
